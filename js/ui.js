import * as model from './model.js'
import * as functions from './functions.js'

// Check button L&F
const checked = 'btn-outline-success'
const unchecked = 'btn-outline-warning'

const success = 'fal fa-check-circle text-success'
const failure = 'fal fa-times-circle text-danger'
const loading = 'fal fa-spinner-third fa-spin text-primary'
const required = 'fas fa-circle text-warning'

const statuses = {required: required}

const reset = $(`#reset`)

let idCounter = 0

document.addEventListener('touchstart', function() {}, true)

$.fn.extend({
  register: function() {
    const check = this.find('.check')

    if (check) {
      check.addClass(unchecked)
      check.on('click', action.toggle)
    }

    this.find('.module').on('click', action.module)
    this.find('.page').on('click', action.next)
    this.find('input').on('change keypress paste input', action.input)
    this.find('select').on('change', action.select)
    this.find('[data-toggle="popover"]').popover()
    this.find('.popover-dismiss').popover({trigger: 'focus'})

    return this
  },
  display: function(...args) {
    this.empty()
    this.append(...args)
    this.register()
    return this
  }
})

// UI Components
const component = {
  check: config => {
    config.icon = config.icon ? config.icon : 'fal fa-check-square'
    return step(fragment('check-template', tmpl => {
      const id = uid(config.check)
      tmpl.siblings('label').text(config.check.name)
        .attr('label-for', id)
        .attr('aria-label', config.check.name)
      tmpl.siblings('button').text(config.check.label)
        .attr('id', id)
      return tmpl
    }), config)
  },
  input: config => {
    config.icon = config.icon ? config.icon : 'fal fa-pencil'
    return step(fragment('input-template', tmpl => {
      tmpl.find('span').text(config.input.label)
      tmpl.children('input')
        .attr('id', uid(config.input))
        .attr('placeholder', config.input.placeholder)
        .attr('aria-label', config.input.label)
        .attr('data-update', config.onchange)
      return tmpl
    }), config)
  },
  select: config => {
    config.icon = config.icon ? config.icon : 'fal fa-list-ul'
    return step(fragment('select-template', tmpl => {
      const select = tmpl.siblings('select')
      const id = uid(config.select)

      tmpl.siblings('label').text(config.select.label)
        .attr('label-for', id)
        .attr('aria-label', config.select.label)
      select.attr('id', id).attr('data-update', config.onchange)
      select.append(component.option('Select...', ''))

      config.select.options.forEach(option => {
        select.append(component.option(option.label, uid(option)))
      })

      return tmpl
    }), config)
  },
  module: config => {
    config.icon = config.icon ? config.icon : 'fal fa-cog'
    return step(fragment('check-template', tmpl => {
      const id = uid(config)

      tmpl.siblings('label').text(config.label)
        .attr('label-for', id)
        .attr('aria-label', config.label)
      tmpl.siblings('button').text('SELECT')
        .attr('id', id)
        .addClass('module')
      return tmpl
    }), config)
  },
  memo: config => {
    return fragment('memo-template', tmpl => {
      tmpl.find('.card-header').text(config.memo.title)
      tmpl.find('.card-text').html(markup(config.memo.text))
      if (config.memo.nav) {
        tmpl.find('#memo-nav').append(component.nav(
          config.memo.nav.page, config.memo.nav.label))
      }
      return tmpl
    })
  },
  delimiter: config => {
    return fragment('delimiter-template', tmpl => {
      tmpl.find('small').text(config.delimiter)
      return tmpl
    })
  },
  tip: config => {
    return fragment('tip-template', tmpl => {
      tmpl.html(markup(config.tip))
      return tmpl
    })
  },
  div: config => {
    return $('<div></div>').attr('id', config.div)
  },
  note: (type, text) => {
    return fragment(`${type}-template`, tmpl => {
      tmpl.find('.note').html(markup(text))
      return tmpl
    })
  },
  help: (title, text) => {
    return fragment('help-template', tmpl => {
      tmpl.attr('data-html', 'true')
        .attr('data-trigger', 'focus')
        .attr('title', title)
        .attr('data-content', markup(text))
      return tmpl
    })
  },
  icon: type => {
    return fragment('icon-template', tmpl => {
      tmpl.addClass(type ? type : 'fas fa-circle invisible')
      return tmpl
    })
  },
  option: (label, value) => {
    return fragment('option-template', tmpl => {
      tmpl.text(label)
      tmpl.val(value)
      return tmpl
    })
  },
  next: config => {
    return fragment('next-template', tmpl => {
      tmpl.text(config.label)
        .attr('id', 'next')
        .attr('data-page', config.page)
        .attr('disabled', !!config.disabled)
      return tmpl
    })
  },
  nav: (page, label) => {
    return fragment('alternate-template', tmpl => {
      tmpl.attr('data-page', page).text(label)
      return tmpl
    })
  },
  fatal: (title, msg) => {
    return fragment('fatal-template', tmpl => {
      tmpl.children('span').text(markup(title))
      tmpl.children('p').text(markup(msg))
      return tmpl
    })
  }
}

// Component actions
const action = {
  toggle: evt => {
    const btn = $(evt.currentTarget)

    if (btn.hasClass(checked)) {
      btn.removeClass(checked)
      btn.addClass(unchecked)
    } else {
      btn.removeClass(unchecked)
      btn.addClass(checked)
    }
  },
  input: evt => {
    const control = $(evt.currentTarget)
    const val = control.val()
    const id = control.attr('id')

    if (!model.update(id, val)) {
      return
    }

    if (control.attr('data-update')) {
      action.run(control.attr('data-update'), control)
    }

    update()
  },
  select: evt => {
    const control = $(evt.currentTarget)
    const selected = control.val()

    if (!selected) {
      return
    }

    const first = $(control).find('>:first-child')

    if (first.val() === '') {
      first.remove()
    }

    if (control.attr('data-update')) {
      action.run(control.attr('data-update'), control)
    }
  },
  module: evt => {
    model.module($(evt.currentTarget).attr('id'))
  },
  next: evt => {
    const target = $(evt.currentTarget).attr('data-page')

    if (!target) {
      model.reset()
      setup()
    } else {
      render(model.page(target))
    }
  },
  run: (f, element) => {
    if (functions[f] instanceof Function) {
      functions[f](element)
    }
  }
}

// Return to and display the setup page.
const setup = () => {
  model.change('setup')
  render(model.page('config'))
}

// Initialise the UI.
const init = () => {
  reset.click(() => {
    model.reset()
    setup()
  })

  setup()
}

const types = ['select', 'input', 'check', 'memo', 'delimiter', 'tip', 'div']

// Render a configuration into a checklist
const render = config => {
  let components = []

  window.scrollTo(0, 0)

  if (!config || !config.elements) {
    fatal('Failed to load page',
      `Something went wrong trying to load the checklist page.`)
    console.error(`invalid config:\n${JSON.stringify(config)}`)
    return
  }

  $('#section-title').text(config.title)

  config.elements.forEach(element => {
    if (!element.disabled) {
      types.forEach(t => {
        if (element[t]) {components.push(component[t](element))}
      })
    }
  })

  let buttons = []

  if (config.next) {
    buttons.push(component.next(config.next))
  } else {
    buttons.push(component.next({page: '', label: 'Done'}))
  }

  if (config.alternatives) {
    config.alternatives.forEach(alternative => {
      buttons.unshift(component.nav(alternative.page, alternative.label))
    })
  }

  $('#content').display(...components)
  $('#buttons').display(...buttons)

  if (config.onload) {
    action.run(config.onload)
  }

  reset.attr('disabled', false)
  update()
}

// Update the UI based on model changes.
const update = () => {
  const updates = model.updates(component)

  for (const [id, value] of Object.entries(updates.values)) {
    $(`#${id}`).val(value)
  }
  for (const [id, msg] of Object.entries(updates.hints)) {
    if (msg) {
      note(id, 'hint', msg)
    }
  }
}

// Display the given elements on the parent element specified by the ID.
const display = (selector, ...elements) => {
  $(`${selector}`).display(...elements)
}

// Toggle a check button.
const toggle = id => {
  const control = $(`#${id}`)

  control.button('toggle')
  action.toggle({currentTarget: control})
}

// Set the status icon for a step.
const status = (id, icon) => {
  $(`#${id}`).parent().siblings('.icons').children(':nth-child(2)')
    .replaceWith(component.icon(icon))
}

// Display a note for a component.
const note = (id, type, msg) => {
  const content = type ? component.note(type, msg) : ''
  $($(`#${id}`).parent().siblings('.notes'))
    .display(content)
}

// Display a fatal error.
const fatal = (title, message) => {
  $('#content').display(component.fatal(title, message))
}

// Enable the 'next' button
const enable = () => {
  $('#next').attr('disabled', false)
}

// Return a step in a checklist by wrapping a control in a configured step.
const step = (control, config) => {
  return fragment('step-template', tmpl => {
    const icons = tmpl.children('.icons')

    tmpl.children('.control').append(control)

    if (config.icon) {
      icons.append(component.icon(config.icon))
    } else {
      icons.append('fal fa-asterisk')
    }

    if (config.help) {
      icons.append(component.help(config.help.title, config.help.text))
    } else {
      icons.append(component.icon(statuses[config.status]))
    }

    if (config.warning) {
      tmpl.children('.notes').append(component.note('warning', config.warning))
    } else if (config.comment) {
      tmpl.children('.notes').append(component.note('comment', config.comment))
    }

    return tmpl
  })
}

// Return an HTML fragment, using the optional function to populate elements of
// the specified template.
const fragment = (template, f) => {
  let definition = $(`#${template}`)

  if (definition.length === 0) {
    console.error(`Template ${template} not found`)
    return
  }

  let tmpl = $(definition.html().trim())
  tmpl = f instanceof Function ? f(tmpl) : tmpl

  if (!tmpl) {
    console.error(`Template function for ${template} did not return a template`)
  }

  return tmpl
}

// Return an ID, either the defined one, or a generated one.
const uid = config => {
  return config.id ? config.id : 'id' + ++idCounter
}

// Escape text and mark it up with some simple markdown-like rules.
const markup = (text) => {
  return $('<div>').text(text).html()
    .replace(/`([^`]+)`/g, '<code>$1</code>')
    .replace(/\|/, '<hr>')
}

export {
  component,
  success,
  failure,
  loading,
  required,
  init,
  render,
  update,
  display,
  toggle,
  status,
  note,
  fatal,
  enable
}
