import * as ui from './ui.js'
import * as model from './model.js'

const err =
  `Sadly, without the configuration file this site is somewhat useless. 
   Hopefully, whatever the problem is it will be fixed shortly, so maybe give it 
   a few minutes and then reload. Otherwise I am not really sure what to 
   suggest. Maybe poke the author?`


$(function() {
  $.ajax({url: 'data.yml'})
    .done(data => {
      try {
        model.init(jsyaml.load(data))
        ui.init()
      } catch (e) {
        ui.fatal("Failed to parse config...", err)
        console.error(e)
      }
    })
    .fail(() => {
      ui.fatal("Failed to load config...", err)
      console.error(arguments)
    })
})
