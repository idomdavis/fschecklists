import * as model from './model.js'
import * as ui from './ui.js'

const base = `https://www.simbrief.com/api/xml.fetcher.php`

const select = element => {
  let modules = []
  model.change(element.val())

  model.modules().forEach(module => {
    modules.push(ui.component.module(module))
  })

  ui.display('#modules', ...modules)
  ui.status('checklist')
  ui.note('checklist')
  ui.enable()
}

const simbrief = () => {
  const user = model.value('integrations', 'simbrief')

  if (!user) { return }

  ui.status('flight-plan', ui.loading)

  $.getJSON(`${base}?username=${user}&json=1`).done(function(data) {
    model.load(data)
    ui.update()
    ui.status('flight-plan', ui.success)
    ui.toggle('flight-plan')
  }).fail(function() {
    ui.display('#content > .step:first-child > .notes',
      ui.component.note('error', 'Failed to load flight plan'))
    ui.status('flight-plan', ui.failure)

  })
}

export { select, simbrief }
