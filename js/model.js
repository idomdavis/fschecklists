import * as data from './data.js'

let selected
let config = {}

let listeners = {
  pax: {
    total: () => {
      values.pax.total = values.pax.total < 0 ? 0 : values.pax.total
      values.pax.economy = values.pax.total - values.pax.business
      pax('economy', 'business')
    },
    economy: () => {
      pax('economy', 'business')
    },
    business: () => {
      pax('business', 'economy')
    }
  },
  weight: {
    cargo: () => {
      const weight = data.number(values.pax.luggage) +
        data.number(values.weight.cargo)
      hints.weight.cargo = `${weight} ${data.units.lbs}`
    }
  },
}

let values = {}
let hints = {}
let inputs = {}

// Initialise the model.
const init = cfg => {
  config = cfg

  reset()

  for (const section of Object.keys(config)) {
    if (config[section].title) {
      config.setup.config.elements[0].select.options.push(
        {label: config[section].title, id: section})
    }
  }

  elements(e => {
    e.forEach(element => {
      if (!element.bind) { return}

      let id = data.find(element, undefined, 'input', 'id')
      id = id ? id : data.find(element, undefined, 'select', 'id')

      if (id) {
        inputs[id] = element.bind
      }
    })
  })
}

const reset = () => {
  values = {
    atc: {},
    modules: {},
    pax: {total: 0, economy: 0, business: 0, luggage: 0},
    temp: {},
    origin: {},
    destination: {},
    flight: {},
    plan: {},
    fuel: {loaded: 0},
    weight: {cargo: 0}
  }

  hints = {
    flight: {},
    plan: {},
    pax: {},
    fuel: {},
    weight: {}
  }

  inputs = {}

  for (const checklist of Object.keys(config)) {
    if (config[checklist].modules) {
      for (let i = 0; i < config[checklist].modules.length; i++) {
        config[checklist].modules[i].selected = false
      }
    }
  }
}

// Load a SimBrief briefing into the model.
const load = plan => {
  const units = data.find(plan, data.units.kgs, 'params', 'units')

  values.pax.total = data.number(data.find(plan, 0, 'general', 'passengers'))
  values.temp.isa = data.number(data.find(plan, 15, 'general', 'avg_temp_dev'))
  values.flight.time = data.number(data.find(plan, 0, 'times', 'est_block'))
  values.origin.runway = data.find(plan, undefined, 'origin', 'plan_rwy')
  values.origin.icao = data.find(plan, undefined, 'origin', 'icao_code')

  values.destination.runway = data.find(plan, undefined, 'destination', 'plan_rwy')
  values.destination.icao = data.find(plan, undefined, 'destination', 'icao_code')

  pax('business', 'economy')

  hints.flight.tail = data.find(plan, undefined, 'api_params', 'fin')
  hints.flight.number = data.find(plan, undefined, 'general', 'flight_number')
  hints.flight.callsign = data.find(plan, undefined, 'general', 'icao_airline')
  hints.flight.route = data.join(values.origin.icao, '/', values.destination.icao)

  hints.plan.crz = data.join(data.find(plan, undefined, 'atc', 'initial_alt'), '/')
  hints.plan.route = data.join(values.origin.icao, '/', values.origin.runway,
    ' ', data.find(plan, undefined, 'general', 'route'), ' ',
    values.destination.icao, '/', values.destination.runway)

  hints.fuel.plan = data.join(data.convert(data.units.lbs,
    data.find(plan, undefined, 'fuel', 'plan_ramp'), units), ' ', data.units.lbs)
  hints.fuel.zfw = data.convert(data.units.tonnes, data.find(plan, undefined,
    'weights', 'est_zfw'), units)
  hints.fuel.block = data.convert(
    data.units.tonnes, values.fuel.loaded, data.units.lbs)

  const duration = data.convert(
    data.units.hours, values.flight.time, data.units.seconds)

  hints.fuel.oil = data.join('Minimum: ', data.convert(data.units.quarts,
    data.oil.min + (duration * data.oil.consumption)), data.units.quarts)

}

// Change the current section.
const change = section => selected = section

// Return the current page.
const page = id => {
  const contents = selected ? data.find(config, {}, selected, id) : {}

  let set = []

  modules().forEach(module => {
    if (module.selected) {
      set.push(module.id)
    }
  })

  contents.elements.forEach((element, i) => {
    if (element.requires) {
      let enabled = false
      element.requires.forEach(required => {
        enabled = enabled || set.includes(required)
      })

      contents.elements[i].disabled = !enabled
    }
  })

  return contents
}

// Update a value in the model.
const update = (id, value) => {
  if (!inputs[id]) { return false}

  const current = data.find(
    values, undefined, inputs[id].section, inputs[id].item)

  if (current === value) {
    return false
  }

  if (!values[inputs[id].section]) {
    values[inputs[id].section] = {}
  }

  values[inputs[id].section][inputs[id].item] = value
  data.find(listeners, () => {
    if (!hints[inputs[id].section]) { hints[inputs[id].section] = {}}
    hints[inputs[id].section][inputs[id].item] = value
  }, inputs[id].section, inputs[id].item)()

  return true
}

// Return the pending updates for the given components.
const updates = components => {
  let updated = {hints: {}, values: {}}

  elements(e => {
    e.forEach(element => {
      const id = data.id(element, components)

      if (id) {

        if (element.hint) {
          const prefix = element.hint.prefix ? element.hint.prefix : ''
          const suffix = element.hint.suffix ? element.hint.suffix : ''
          updated.hints[id] = data.join(prefix, data.find(hints, undefined,
            element.hint.section, element.hint.item), suffix)
        }

        if (element.bind) {
          updated.values[id] = data.find(values, '', element.bind.section,
            element.bind.item)
        }
      }
    })
  })

  return updated
}

const module = id => {
  if (!config[selected].modules) { return }

  config[selected].modules.forEach((module, i) => {
    if (module.id === id) {
      config[selected].modules[i].selected =
        !config[selected].modules[i].selected
    }
  })
}

// Return the set of modules.
const modules = () => {
  if (!config[selected]) { return [] }

  return config[selected].modules ? config[selected].modules : []
}

// Return the given value.
const value = (section, item) => {
  return data.find(values, undefined, section, item)
}

// Iterate through the elements running the given function.
const elements = f => {
  for (const checklist of Object.keys(config)) {
    for (const section of Object.keys(config[checklist])) {
      for (const part of Object.keys(config[checklist][section])) {
        if (part === 'elements') {
          f(config[checklist][section][part])
        }
      }
    }
  }
}

// Passenger calculations.
const pax = (primary, secondary) => {
  values.pax[primary] = data.number(values.pax[primary])
  values.pax[primary] = values.pax[primary] < 0 ? 0 : values.pax[primary]
  values.pax.total = values.pax[primary] > values.pax.total ?
    values.pax[primary] : values.pax.total
  values.pax[secondary] = values.pax.total - values.pax[primary]
  values.pax.luggage = values.pax.total * data.weights.luggage

  hints.pax.business =
    `${values.pax.business * data.weights.pax} ${data.units.lbs}`
  hints.pax.economy =
    `${values.pax.economy * data.weights.pax} ${data.units.lbs}`
  listeners.weight.cargo()
}

// Shut the linter up. All these properties come from config files.
const dummy = {
  section: undefined,
  temp: undefined,
  item: undefined,
  suffix: undefined,
  modules: undefined,
  alternatives: undefined,
  warning: undefined,
  comment: undefined,
  page: undefined,
  requires: undefined
}

if (dummy.pax) { console.log() }

export {
  init,
  reset,
  load,
  change,
  page,
  update,
  updates,
  module,
  modules,
  value
}
