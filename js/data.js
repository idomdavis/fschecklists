// Units
const units = {
  lbs: 'lbs',
  kgs: 'kgs',
  tonnes: 't',
  quarts: 'qt',
  hours: 'h',
  seconds: 's'
}

// Weights in lbs for calculating load
const weights = { pax: 175, luggage: 55 }

const oil = {min: 9.5, consumption: 0.5}

// Values used to convert between units
const conversion = {
  kgs: {lbs: 0.453592, t: 1000},
  lbs: {kgs: 2.20462, t: 2204.62},
  t: {kgs: 0.001, lbs: 0.000453592, precision: 1},
  h: {s: 1/3600, precision: 2},
  qt: { precision: 1}
}

// Find the ID of a nested component.
const id = (obj, children) => {
  for (const key of Object.keys(children)) {
    if (obj[key] && obj[key].id) {
      return obj[key].id
    }
  }

  return undefined
}

// Find a nested property, returning the fallback if it's not found.
const find = (obj, fallback, ...path) => {
  return path.reduce((acc, v) =>
    (acc != null && acc[v] != null) ? acc[v] : fallback, obj)
}

// Joins the arguments into a string. Returns undefined if any argument is
// undefined.
const join = (...args) => {
  let msg = ''

  args.forEach(arg => {
    msg = arg == null || msg === undefined ? undefined : msg + arg
  })

  msg = msg ? msg.trim() : undefined
  return msg ? msg : undefined
}


// Convert a value defined in units into unit.
const convert = (unit, value, units) => {
  if (!number(value) === undefined) { return undefined }

  let scalar
  let precision
  let lookup = conversion[unit]

  if (lookup) {
    scalar = lookup[units]
    precision = lookup.precision
  }

  scalar = scalar ? scalar : 1
  precision = precision ? precision : 0
  value = value * scalar
  value = value >= 0 ? value : 0

  return isNaN(value) ? undefined : Number(value).toLocaleString(undefined,
    {
      maximumFractionDigits: precision,
      minimumFractionDigits: precision,
    })
}

// For a value to be a number, returning 0 if it's not a number.
const number = n => {
  return isNaN(+n) ? undefined : +n
}

export { units, weights, oil, id, find, join, convert, number}
